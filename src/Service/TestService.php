<?php
// src/Service/MessageGenerator.php
namespace App\Service;

use OldSound\RabbitMqBundle\RabbitMq\Consumer;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

use Doctrine\ORM\EntityManager;


class TestService implements ConsumerInterface
{

	protected EntityManager $entityManager;
	protected $producer;

    public function __construct(
		EntityManager $entityManager,
		$producer
    ) {
		$this->entityManager = $entityManager;
		$this->producer = $producer; //producer no 
    }
    // ...

	public function export(){
		dump("hola dola!");
	}

	/*public function getConnectionParameters() {
        return array('vhost' => $this->getVhost());
    }*/

	public function execute(AMQPMessage $msg){
		dump("hola: ", $msg->body);
		dump('----------------');
		// dump("hola constructor", $this->entityManager, $this->producer);
		// dump('----------------');
	}

}